## Project setup
npm install or yarn install
 
### Compiles and hot-reloads for development
npm run dev or yarn run dev
 
### Compiles and minifies for production
npm run build or yarn run build
 
### basic Usage 
Editor Template
equivalent use Vue.js with mustache around variable {{ variable }}
all data - {{ data }} so you can use {{ data.variable }}
For the template code there are available functions: 
 alert() and console() - équivalent de console.log 
 
Editor Style
same style for all elements (without the use of class and id) 
 
Editer Data
Input data represents an array of objects. ex: 
[{"val":4,"mal":5,"bar":9, “msg”: “Sum of val et mal”},
{"val":8,"mal":3,"bar":11, “msg”: “Sum of val et mal”}]
