
export const ref = {

  state: {
    refresh: true
  },
  actions: {
    passerFalse(context) {
      context.commit("update")
    },
    passerTrue(context) {
      context.commit("initial")
    },
  },

  mutations: {
    update(state) {
      state.refresh = false
    },
    initial(state) {
      state.refresh = true
    }

  }
};
