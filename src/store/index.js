import { createStore } from "vuex";
import { auth } from "./auth.module";
import { ref } from "./ref.module";

const store = createStore({
  modules: {
    auth,
    ref,
  },
});

export default store;
