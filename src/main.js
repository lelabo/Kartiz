import { createApp } from 'vue'
import App from './App.vue'
import ElementPlus from 'element-plus'
import 'element-plus/theme-chalk/index.css'
import 'element-plus/theme-chalk/display.css';
import router from './router'
import store from './store'
import { FontAwesomeIcon } from './plugins/font-awesome'

createApp(App)
  .use(router)
  .use(store)
  .use(ElementPlus)
  .component("font-awesome-icon", FontAwesomeIcon)
  .mount('#app')