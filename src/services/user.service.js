import axios from 'axios';
import authHeader from './auth-header';

const API_URL = `https://kartiz.herokuapp.com/cards/`;
const API_URL_Post= `https://kartiz.herokuapp.com/cards/`;

class UserService {
 getPublicContent() {
    return  axios.get(API_URL , { headers: authHeader() });
  }
  getPosts(){
    
     return axios.get(API_URL_Post, { headers: authHeader() })
   
    }

 }

export default new UserService();
