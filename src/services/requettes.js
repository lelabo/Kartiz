import axios from 'axios';
import authHeader from './auth-header';

//const API_URL = 'http://localhost:5000/cards/';
const API_URL = 'https://kartiz.herokuapp.com/cards/';


class ProjectService {
  putApiProjectId(projectId, dataArray, template, style, userId) {

    return axios.put(API_URL + `update/${projectId}`, {
      // name: this.cardName,
      dataArray: dataArray,
      template: template,
      style: style,
      userId: userId,
    }, { headers: authHeader() })
  }
  postApiProjectId(projectName, userId) {
    return axios.post(API_URL + `register`, {
      name: projectName,
      userId: userId,
    })

  }

  async deleteApiProjectId(projectId) {
    return await axios.delete(API_URL + `delete/${projectId}`, { headers: authHeader() })

  }

  reqApiproject() {
    return axios.get(
      API_URL, { headers: authHeader() }
    );
  }
  async reqApiprojectId(projectId) {
    return await axios.get(
      API_URL + `${projectId}`, { headers: authHeader() }
    );
  }
}

export default new ProjectService();