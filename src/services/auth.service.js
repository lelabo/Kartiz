import axios from 'axios';


// const API_URL = 'http://localhost:5000/auth/';
const API_URL = 'https://kartiz.herokuapp.com/auth/'

class AuthService {
  login(user) {
    return axios
      .post(API_URL + 'login', {
        email: user.email,
        password: user.password
      })
      .then(response => {
        if (response.data.accessToken) {
          localStorage.setItem('user', JSON.stringify(response.data));
        }
        return response.data;
      });
  }
  
  logout() {
    localStorage.removeItem('user');
  }
  register(user) {
    return axios.post(API_URL + 'register', {
      email: user.email,
      password: user.password,
      pseudo: user.pseudo,
    });
  }

  forgot(user) {
    return axios.post(API_URL + 'forgot', {
      email: user.email,
    })
  }
  reset(user) {
    var str = document.location.href;
    var url = new URL(str);
    var token = url.searchParams.get("token");
   
    return axios.post(API_URL + 'reset', {
      password: user.password,
      email: user.email,
      token: token
    }) .then(response => {
      return response.data;
    });

  }
}

export default new AuthService();
