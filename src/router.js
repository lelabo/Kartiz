import { createRouter, createWebHistory } from 'vue-router'

import Login from './components/Authentication/LoginPage.vue'
import HomePage from './components/HomePage.vue'
import SignupPage from './components/Authentication/SignupPage.vue'
import Container from './components/Container.vue'
import CardCollection from './components/CardCollection.vue'
import Project from './components/Project.vue'
import NewProjectPage from './components/NewProjectPage.vue'
import EditorCode from './components/Editors/EditorCode.vue'
import EditorStyle from './components/Editors/EditorStyle.vue'
import EditorData from './components/Editors/EditorData.vue'
import ForgotPassword from './components/Authentication/ForgotPasswordPage.vue'
import ResetPassword from './components/Authentication/ResetPasswordPage.vue'


const base = import.meta.env.BASE_URL

const routes = [
    {
        path: '/',
        name: 'Home',
        component: HomePage
    },
    {
        path: '/login',
        name: 'Login',
        component: Login
    },

    {
        path: '/signup',
        name: 'SignupPage',
        component: SignupPage
    },
    {
        path: '/container',
        name: 'Container',
        component: Container,
    },

    {
        path: "/project/:id",
        name: "Project",
        component: Project,
        props: true
    },

    {
        path: "/forgotPassword",
        name: "ForgotPassword",
        component: ForgotPassword,
      
    },
    {
        path: "/resetPassword",
        name: "ResetPassword",
        component: ResetPassword,
      
    },
    {
        path: "/editorHtml",
        name: "editorHtml",
        component: EditorCode,
      
    },
    {
        path: "/editorStyle",
        name: "editorStyle",
        component: EditorStyle,
      
    },
    {
        path: "/editorData",
        name: "editorData",
        component: EditorData,
      
    },
    {
        path: "/cardCollection",
        name: "cardCollection",
        component: CardCollection,
      
    },
        {
      path: '/newProject',
      name: "NewProject",
      component: NewProjectPage,
     
    },



    


]
const routerHistory = createWebHistory(base)
const router = createRouter({
    history: routerHistory,
    routes
})

export default router